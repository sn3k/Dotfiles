"Unmap the arrow keys
ino <down> <Nop>
ino <left> <Nop>
ino <right> <Nop>
ino <up> <Nop>
no <down> <Nop>
no <left> <Nop>
no <right> <Nop>
no <up> <Nop>

"Line numbers are cool
set number
set relativenumber 

"Easier movement between splits
nnoremap <C-J> <C-W><C-J> 
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

"Copy to clipboard, because "+ register doesn't work on Suse
vmap <leader>xy :!xclip -f -sel clip<CR>
map <leader>xp mz:-1r !xclip -o -sel clip<CR>`z

"Encoding duh
set encoding=utf-8

"Folding
set foldmethod=indent "for python development indent method is the best. Consider using SimplyFold plugin with that!
set foldlevel=1
set foldlevelstart=1

" Netrw settings
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
map <leader>f <Esc>:30vs +Ex<CR> 

" Bootleg fuzzy-finder options
set path+=**
set wildmenu
set ignorecase

""""Status line"""""""
 set laststatus=2 "status line always visible

"VUNDLE AND PLUGINS
set nocompatible              " this is not Vi from 1976 after all
"filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'vim-flake8'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-commentary'
Plugin 'Yggdroot/indentLine'
Plugin 'tpope/vim-fugitive'
Plugin 'vimwiki/vimwiki'
Plugin 'vim-airline'
call vundle#end()            " required
filetype plugin indent on    " required
filetype plugin on    " required
"to install plugin run :PluginInstall
"to update plugin run :PluginUpdate

let g:airline_powerline_fonts=1
syntax enable
" colorscheme Dark
colorscheme slate
set cursorline
set colorcolumn=80
hi ColorColumn ctermbg=darkgrey

map <F6> :!pdflatex % <Enter> 
map <F7> :!pandoc % -s -o '%:r.pdf' <Enter> 
map <F8> :setlocal spell! spelllang=en_gb,pl<CR>
map <F1> :colo slate<CR>
map <F2> :colo koehler<CR>
noremap <Space><Space> /<??><Enter>"_c4l


"Latex mapping
autocmd Filetype tex inoremap ;b \textbf{}<Space><??><Esc>T{i
autocmd Filetype tex inoremap ;i \textit{}<Space><??><Esc>T{i
autocmd Filetype tex inoremap ;un \underline{<??>}<Space><??><Esc>T{i
autocmd FileType tex inoremap ;ct \textcite{}<Space><??><Esc>T{i
autocmd FileType tex inoremap ;ref \ref{}<Space><??><Esc>T{i
autocmd Filetype tex inoremap ;ul \begin{itemize}<Enter><Enter>\end{itemize}<Enter><Enter><??><Esc>3kA<Space><Space>\item<Space>
autocmd Filetype tex inoremap ;ol \begin{enumerate}<Enter><Enter>\end{enumerate}<Enter><Enter><??><Esc>3kA<Space><Space>\item<Space>
autocmd Filetype tex inoremap ;it <Space><Space>\item<Space>
autocmd Filetype tex inoremap ;wf \begin{wrapfigure}{l}{8cm}<Enter>\begin{center}<Enter>\includegraphics[width=\linewidth,keepaspectratio]{<??>}<Enter>\caption{<??>}<Enter>\label{<??>}<Enter>\end{center}<Enter>\end{wrapfigure}<Enter>
autocmd Filetype tex inoremap ;fi \begin{figure}[h!]<Enter>\begin{center}<Enter>\includegraphics[width=\linewidth,keepaspectratio]{<??>}<Enter>\caption{<??>}<Enter>\label{<??>}<Enter>\end{center}<Enter>\end{figure}<Enter>
autocmd FileType tex inoremap ;sec \section{}<Enter><Enter><??><Esc>2kf}i
autocmd Filetype tex inoremap ;fr %FRAME<Enter>\begin{frame}<Enter>\frametitle{<??>}<Enter><??><Enter><Enter><Enter>\end{frame}<Enter><Enter><Esc>9kA
autocmd Filetype tex inoremap ;eq $$<Esc>i
autocmd Filetype tex inoremap ;rar $\rightarrow$<Space>
autocmd Filetype tex inoremap ;Rar $\Rightarrow$<Space>
autocmd Filetype tex inoremap ;lar $\leftarrow$<Space>
autocmd Filetype tex inoremap ;Lar $\Leftarrow$<Space>

"PEP8 for python
"autocmd BufWritePost *.py call Flake8()
autocmd FileType python map <buffer> <F3> :call Flake8()<CR>

set tabstop=4  softtabstop=4   shiftwidth=4   textwidth=0   expandtab   fileformat=unix 

"autocmd FileType python setlocal textwidth=79
autocmd FileType tex setlocal noai nocin nosi inde=  " who needs autoindent in tex files anyway? abbrev. noautoindent, nocindent, nosmartindent, indentexpr=

autocmd FileType yaml setlocal ai ts=2 sw=2 et
"Default tabs
"set tabstop=8  softtabstop=8   shiftwidth=8   textwidth=79   noexpandtab   fileformat=unix 

